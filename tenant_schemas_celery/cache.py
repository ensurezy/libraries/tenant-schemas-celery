from datetime import datetime, timedelta

from gevent import threading


class _CacheEntry(object):
    def __init__(self, key, value, expires_at):
        self.key = key
        self.value = value
        self.expires_at = expires_at


class SimpleCache(object):
    def __init__(self, storage=None):
        self.lock = threading.Lock()
        self.__items = storage if storage is not None else {}

    def get(self, key, default):
        with self.lock:
            if key not in self.__items:
                return default
            elif self.__items[key].expires_at < datetime.utcnow():
                self.__items.pop(key, None)
                return default

            return self.__items[key].value

    def set(self, key, value, expire_seconds):
        with self.lock:
            self.__items[key] = _CacheEntry(
                key=key,
                value=value,
                expires_at=datetime.utcnow() + timedelta(seconds=expire_seconds),
            )
