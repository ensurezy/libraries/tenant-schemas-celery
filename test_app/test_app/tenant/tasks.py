from celery_app import app
from django_tenants.routers import get_connection


@app.task
def print_schema():
    connection = get_connection()
    print(connection.schema_name)


@app.task
def periodic_print_schema():
    print_schema()
